Webform as block
----------------

### About this Module

This Module provides a way for site builders to display webforms as blocks from the UI.
Other ways to embed a webform can be found [here](https://www.drupal.org/docs/8/modules/webform/webform-cookbook/how-to-embed-a-webform) 

### Installing the Webform Module

1. Downlaod and enable the module.
2. Goto "Blocks" tab on **Structure > Webforms** _(admin/structure/webform/blocks)_.
3. Select the webforms to be available as block.
4. Place the enabled block(s) (admin/structure/block).